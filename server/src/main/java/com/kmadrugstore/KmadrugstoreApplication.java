package com.kmadrugstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KmadrugstoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(KmadrugstoreApplication.class, args);
    }

}
